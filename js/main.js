// navbar color change on scroll
$(window).scroll( () => {
    $('nav.top-nav').toggleClass('scrolled', $(this).scrollTop() > 50)
});


if ( $(window).width() >= 768 ) {
    // Award Carousel
    $('.award-carousel').owlCarousel({
        items: 2,
        autoplay: true,
        loop:true,
        margin:20,
        nav:false,
        dots: true,
        responsiveClass:true,

    });


    // Service Carousel
    $('.service-carousel').owlCarousel({
        stagePadding:100,
        items: 1,
        autoplay: true,
        loop:true,
        margin:10,
        nav:true,
        dots: false,
        responsiveClass:true,
        navText: ['<img alt="" src="./img/service-prev.png">','<img alt="" src="./img/service-next.png">']
    })
}





var speed = "500";
var action = "click";

$(document).ready(function(){
  $('li.q').on(action ,function(){
    $(this).next()
      .slideToggle(speed, function(){

      })
      .siblings('li.a').slideUp();
    
    // find active class
    var current = $(this);
    
    $('li.q').not(current).removeClass('active');
    
    current.toggleClass('active');
      
  });
});



// SLIDER REVOLUTION
jQuery('.tp-banner-full').show().revolution({
    dottedOverlay: "none",
    delay: 16000,
    startwidth: 1170,
    startheight: 700,
    hideThumbs: 200,

    thumbWidth: 100,
    thumbHeight: 50,
    thumbAmount: 5,

    navigationType: "bullet",
    navigationArrows: "solo",
    navigationStyle: "preview1",

    touchenabled: "on",
    onHoverStop: "on",

    swipe_velocity: 0.7,
    swipe_min_touches: 1,
    swipe_max_touches: 1,
    drag_block_vertical: false,

    parallax: "mouse",
    parallaxBgFreeze: "on",
    parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],

    keyboardNavigation: "off",

    navigationHAlign: "center",
    navigationVAlign: "bottom",
    navigationHOffset: 0,
    navigationVOffset: 20,

    soloArrowLeftHalign: "left",
    soloArrowLeftValign: "center",
    soloArrowLeftHOffset: 20,
    soloArrowLeftVOffset: 0,

    soloArrowRightHalign: "right",
    soloArrowRightValign: "center",
    soloArrowRightHOffset: 20,
    soloArrowRightVOffset: 0,

    shadow: 0,
    fullWidth: "on",
    fullScreen: "on",

    spinner: "spinner4",

    stopLoop: "off",
    stopAfterLoops: -1,
    stopAtSlide: -1,

    shuffle: "off",

    autoHeight: "off",
    forceFullWidth: "off",



    hideThumbsOnMobile: "off",
    hideNavDelayOnMobile: 1500,
    hideBulletsOnMobile: "off",
    hideArrowsOnMobile: "off",
    hideThumbsUnderResolution: 0,

    hideSliderAtLimit: 0,
    hideCaptionAtLimit: 0,
    hideAllCaptionAtLilmit: 0,
    startWithSlide: 0,
    videoJsPath: "rs-plugin/videojs/",
    fullScreenOffsetContainer: ""
});





// SLIDER REVOLUTION
jQuery('.mobile-slider').show().revolution({
    dottedOverlay: "none",
    delay: 16000,
    startwidth: 1170,
    // startheight: 700,
    hideThumbs: 200,
    // responsiveLevels:[4096,1024,778,480],
    
    thumbWidth: 100,
    thumbHeight: 50,
    thumbAmount: 5,
    
    navigationType: "bullet",
    navigationArrows: "solo",
    navigationStyle: "preview1",
    
    touchenabled: "on",
    onHoverStop: "on",
    
    swipe_velocity: 0.7,
    swipe_min_touches: 1,
    swipe_max_touches: 1,
    drag_block_vertical: false,
    
    parallax: "mouse",
    parallaxBgFreeze: "on",
    parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
    
    keyboardNavigation: "off",
    
    navigationHAlign: "center",
    navigationVAlign: "bottom",
    navigationHOffset: 0,
    navigationVOffset: 20,
    
    soloArrowLeftHalign: "left",
    soloArrowLeftValign: "center",
    soloArrowLeftHOffset: 20,
    soloArrowLeftVOffset: 0,
    
    soloArrowRightHalign: "right",
    soloArrowRightValign: "center",
    soloArrowRightHOffset: 20,
    soloArrowRightVOffset: 0,
    
    shadow: 0,
    fullWidth: "on",
    fullScreen: "on",
    fullScreenAlignForce:"on",
    
    spinner: "spinner4",
    
    stopLoop: "off",
    stopAfterLoops: -1,
    stopAtSlide: -1,
    
    shuffle: "off",
    
    autoHeight: "on",
    forceFullWidth: "off",
    
    
    
    hideThumbsOnMobile: "off",
    hideNavDelayOnMobile: 1500,
    hideBulletsOnMobile: "off",
    hideArrowsOnMobile: "off",
    hideThumbsUnderResolution: 0,
    
    hideSliderAtLimit: 0,
    hideCaptionAtLimit: 0,
    hideAllCaptionAtLilmit: 0,
    startWithSlide: 0,
    videoJsPath: "rs-plugin/videojs/",
    fullScreenOffsetContainer: ""
});


// jQuery for page scrolling feature - requires jQuery Easing plugin
$('body').on('click', '.page-scroll a', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top - 46 )
    }, 1500, 'easeInOutExpo');
    event.preventDefault();
});

$("body").scrollspy({target: ".navbar-collapse", offset:200});


// Language change
$('#language-change').on('change', function() {
    location.href = $(this).val()
});


$(document).ready(function() {
    // Animate loader off screen
    $(".se-pre-con-wraper").fadeOut("slow");
});